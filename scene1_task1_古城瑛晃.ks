
;■条件
;・使用キャラクターは自由
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・急に話が飛んでいる場合に、場所などをユーザーが理解できるようにスクリプトが組めるか
;・キャラ性を理解できているか

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

;窶披披披披披披披蝿ﾈ下よりスクリプトを組んでください窶披披披披披披披髏

@layopt layer=message0 visible=false


;キャラ１定義
[chara_new name="akane" storage="chara/akane/normal.png" jname="キャラ１"]

;キャラ２定義
[chara_new name="yamato" storage="chara/yamato/normal.png" jname="キャラ２"]


;キャラ１表情
[chara_face name="akane" face="happy" storage="chara/akane/happy.png" jname="キャラ１"]
[chara_face name="akane" face="normal" storage="chara/akane/normal.png" jname="キャラ１"]
[chara_face name="akane" face="angry" storage="chara/akane/angry.png" jname="キャラ１"]

;キャラ２表情
[chara_face name="yamato" face="tohoho" storage="chara/yamato/tohoho.png" jname="キャラ２"]
[chara_face name="yamato" face="angry" storage="chara/yamato/angry.png" jname="キャラ２"]

;背景



[playbgm storage="normal.ogg"]
[playse storage="suzume.ogg"]
[bg storage="sakamiti.jpg"]
[chara_show name="akane" left=280 top=50]
@layopt layer=message0 visible=true
[wait time=200]
;メニューボタン
@showmenubutton


#キャラ１ 
キャラ２！[r]
早く早くー！[l]

[cm]

[chara_mod name="akane"  face="angry"]

#キャラ１ 
そんなのんびりしてると、遅刻しちゃうぞ！[l]

[cm]

[stopse]

[chara_hide name="akane"]


[chara_mod name="yamato"  face="angry"]
[chara_show name="yamato" left=280 top=0]


#キャラ２ 
……分かってるって！[r]
この坂道きついんだよ！[l]

[cm]

[chara_hide name="yamato"]
[layopt layer="message0" visible="false"]


[bg storage="gaikan_hiru.jpg" method="rollIn" time="2200"]
[bg storage="rouka.jpg" time="1500"]

[playse storage=class_door1.ogg"]


[bg storage="room.jpg" method="slideInLeft" time="1500"]
[chara_mod name="akane"  face="normal"]




[chara_show name="akane" time="0" left=1000 top=50]

[chara_move name="akane" anim="true" time="1000" effect="easeOutQuad" wait="true" left="0" ]
[wa]
[anim name=akane left="+=50" time=200 effect="easeInSine"]
[wa]
[wait time=500]


[layopt layer="message0" visible="true" ]

#キャラ１ 
到着！[l]

[cm]

[chara_mod name="akane"  face="happy"]

#キャラ１ 
みんな、おっはよー！[l]

[cm]
[chara_show name="yamato" time="0" face="tohoho" left=1000 top=0]
[chara_move name="yamato" anim="true" time="3000" effect="easeOutQuad" wait="true" left="450" ]

#キャラ２ 
はぁはぁ……[l]

[cm]

[chara_mod name="yamato"  face="angry"]

#キャラ２ 
置いてくんじゃねえよ……[r]
この体力馬鹿が！[l]

[cm]

[chara_mod name="akane"  face="normal"]

#キャラ１ 
ほら！ [r]
授業始まっちゃうよー[l]

[cm]

#キャラ１ 
席についてー[l]

[cm]

[chara_mod name="yamato"  face="tohoho"]

#キャラ２ 
はあ……わかったよ[l]

[mask]
[chara_hide_all]
[chara_hide_all layer=1]

[stopbgm]

[freeimage layer="base"]
#
@layopt layer=message0 visible=false
[mask_off]
[jump first.ks]


 