
;■条件
;・キャラクターはあかね、やまとを使用
;・やまとの表情はnormal、angry使用不可
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・真剣な表情がない中でどう演出するか
;・後半の靴の話でどのような演出をするか

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

;————————以下よりスクリプトを組んでください————————

@layopt layer=message0 visible=true

;キャラ１定義
[chara_new name="akane" storage="chara/akane/normal2.png" jname="キャラ１"]

;キャラ２定義
[chara_new name="yamato" storage="chara/yamato/tohoho2.png" jname="キャラ２"]

;キャラ１表情
[chara_face name="akane" face="happy" storage="chara/akane/happy2.png" jname="キャラ１"]
[chara_face name="akane" face="doki" storage="chara/akane/doki2.png" jname="キャラ１"]

;キャラ２表情
[chara_face name="yamato" face="sad" storage="chara/yamato/sad2.png" jname="キャラ２"]
[chara_face name="yamato" face="happy" storage="chara/yamato/happy2.png" jname="キャラ２"]

;背景
[bg storage="gaikan_hiru.jpg" width="1600" height="1200" method="fadeIn"]

;BGM：通常
[playbgm storage="normal.ogg"]

;メニューボタン
@showmenubutton
[chara_show name="akane" time="0" left=1000 top=50]

[chara_show name="yamato" face="sad" left=50 top=0]

[chara_move name="akane" anim="true" time="1000" effect="easeOutQuad" wait="true" left="500" ]


#akane:doki
#キャラ１ 
キャラ２がすごく真剣な顔をしてる……。何あったの！？[l]
[cm]

#キャラ２
SNSにどの写真載せようか悩んでるんだよ……[l]
[cm]


#akane:happy
#キャラ１ 
楽しそうなの載せたら良いと思うな！[l]
[cm]
#yamato:happy
#キャラ２ 
じゃあ一緒に撮ろうぜ！[l]
[cm]
#akane:default
#キャラ１ 
でも、ただ載せるんじゃ面白くないよ？[l]
[cm]
#yamato:default
#キャラ２ 
じゃあ……[l]
[cm]
#akane:happy
#キャラ１ 
あ！　キャラ２新しい靴履いてたよね！[r]
私も買ったばかりだから、靴履いて写真撮ってみない？[l]
[cm]

[mask]
[chara_hide_all time="0"]

[chara_show name="akane" time="0" width="300" height="600" top="50" left="400" zindex=2]
[chara_show name="yamato" time="0" width="562.5" height="675" top="0" left="100" zindex=1 face="happy"]

[camera time="0" zoom="1.5" y="100"]
[mask_off]

#キャラ２ 
じゃあ、撮るぜ！[l]
[cm]
#akane:doki
#キャラ１ 
ちょっと待て！　やっぱり、もう少し靴を近くした方がよくない？[l]
[cm]

[camera from_zoom="1.5" zoom="1.2" from_y="100" y="0"]




#キャラ２ 
こんな感じか？[l]
[cm]
#akane:happy
#キャラ２ 
なら……はい、チーズ[l]
[cm]

[mask]

[chara_move name="akane" time="0" wait="true" width="400" height="800" top="50" left="375"]
[chara_move name="yamato" time="0" wait="true" width="750" height="900" top="0" left="25"]
[reset_camera time="0"]


[mask_off]

#キャラ１ 
いい感じの写真撮れた？[l]
[cm]

#キャラ２ 
これならSNSに載せられそうだな！[l]
[cm]

[stopbgm]

[mask]
[chara_hide_all]
[chara_hide_all layer=1]
[freeimage layer="base"]
#
@layopt layer=message0 visible=false
[mask_off]
[jump storage=title.ks]

 