
;■条件
;・キャラクターはあかね、やまとを使用
;・やまとの表情はnormal、angry使用不可
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・真剣な表情がない中でどう演出するか
;・後半の靴の話でどのような演出をするか

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

;————————以下よりスクリプトを組んでください————————

@layopt layer=message0 visible=true

;#あかね定義

[chara_new name="akane" storage="chara/akane/normal.png" jname="あかね"]

;#やまと定義

[chara_new name="yamato" storage="chara/yamato/tohoho.png" jname="やまと"]


;#あかね表情

[chara_face name="akane" face="normal" storage="chara/akane/normal.png" jname="あかね"]

[chara_face name="akane" face="happy" storage="chara/akane/happy.png" jname="あかね"]

[chara_face name="akane" face="doki" storage="chara/akane/doki.png" jname="あかね"]

[chara_face name="akane" face="sad" storage="chara/akane/sad.png" jname="あかね"]

[chara_face name="akane" face="angry" storage="chara/akane/angry.png" jname="あかね"]


;#やまと表情

[chara_face name="yamato" face="sad" storage="chara/yamato/sad.png" jname="やまと"]

[chara_face name="yamato" face="tohoho" storage="chara/yamato/tohoho.png" jname="やまと"]

[chara_face name="yamato" face="happy" storage="chara/yamato/happy.png" jname="やまと"]
;背景
[bg storage="gaikan_hiru.jpg" time="1500"]

;メニューボタン
@showmenubutton

[playbgm storage="normal.ogg"]

[chara_mod name="akane"  face="normal"]

[chara_show name="akane" left=1000 top=50]

[anim name=akane left="-=750" time=2000 effect="easeInSine"]

[wa]

[chara_mod name="akane"  face="angry"]


#あかね
やまとがすごく真剣な顔をしてる……。何あったの！？[l]

[cm]

[chara_hide_all]

[chara_mod name="akane"  face="angry"]

[chara_mod name="yamato"  face="tohoho"]

[chara_show name="akane" left=450 top=80]

[chara_show name="yamato" left=50 top=20]

#やまと
SNSにどの写真載せようか悩んでるんだよ……[l]

[cm]

[chara_mod name="akane"  face="happy"]

#あかね
楽しそうなの載せたら良いと思うな！[l]

[cm]

[chara_mod name="yamato"  face="happy"]

#やまと
じゃあ一緒に撮ろうぜ！[l]

[cm]

[chara_mod name="akane"  face="normal"]

#あかね
でも、ただ載せるんじゃ面白くないよ？[l]

[cm]

[chara_mod name="yamato"  face="sad"]

#やまと
じゃあ……[l]

[cm]

[chara_mod name="akane"  face="happy"]


[anim name=akane top="-=20" time=100 effect="easeInSine"]

[wa]

[anim name=akane top="+=20" time=100 effect="easeInSine"]

[wa]

#あかね
あ！　やまと新しい靴履いてたよね！[r]
私も買ったばかりだから、靴履いて写真撮ってみない？[l]

[cm]


[chara_mod name="yamato"  face="happy"]

#やまと
じゃあ、撮るぜ！[l]

[cm]

[chara_hide_all]

[bg storage="kuro.jpg" time="500"]

#あかね
ちょっと待て！　やっぱり、もう少し靴を近くした方がよくない？[l]

[cm]

[bg storage="gaikan_hiru.jpg" time="1500"]

[chara_mod name="akane"  face="angry"]

[chara_mod name="yamato"  face="tohoho"]

[chara_show name="akane" left=450 top=80]

[chara_show name="yamato" left=50 top=20]


#やまと
こんな感じか？[l]

[anim name=yamato left="+=50" time=500 effect="easeInSine"]

[wa]

[cm]

[wait time=500]

[chara_mod name="akane"  face="happy"]

[anim name=akane top="+=20" time=200 effect="easeInSine"]

[wa]

[anim name=akane top="-=20" time=200 effect="easeInSine"]

[wa]

[chara_mod name="yamato"  face="happy"]

#やまと
なら……はい、チーズ[l]

@layopt layer=message0 visible=false

[chara_hide_all]

[cm]

[playse storage="landing.ogg"]

[bg storage="kuro.jpg" time="500"]

[bg storage="siro.jpg" time="100"]

[bg storage="gaikan_hiru.jpg" time="1500"]

[cm]

[chara_mod name="akane"  face="happy"]

[chara_mod name="yamato"  face="happy"]

[chara_show name="akane" left=450 top=80]

[chara_show name="yamato" left=100 top=20]


@layopt layer=message0 visible=true

[chara_mod name="akane"  face="normal"]

#あかね
いい感じの写真撮れた？[l]

[cm]

[anim name=yamato top="+=30" time=100 effect="easeInSine"]

[wa]

[anim name=yamato top="-=30" time=100 effect="easeInSine"]


#やまと
これならSNSに載せられそうだな！[l]

[cm]

@layopt layer=message0 visible=false

[mask]

[stopbgm]

[chara_hide_all]
[chara_hide_all layer=1]
[freeimage layer="base"]
#
[mask_off]
[jump first.ks]


 