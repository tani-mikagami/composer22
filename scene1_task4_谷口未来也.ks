
;■条件
;・キャラクターはあかね、やまとを使用
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・鏡を見ている際にどのような演出をするか

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

@layopt layer=message0 visible=true

;————————以下よりスクリプトを組んでください————————

@layopt layer=message0 visible=true

;キャラ１定義
[chara_new name="akane" storage="chara/akane/normal2.png" jname="キャラ１"]

;キャラ２定義
[chara_new name="yamato" storage="chara/yamato/normal2.png" jname="キャラ２"]

;キャラ１表情
[chara_face name="akane" face="happy" storage="chara/akane/happy2.png" jname="キャラ１"]
[chara_face name="akane" face="angry" storage="chara/akane/angry2.png" jname="キャラ１"]
[chara_face name="akane" face="doki" storage="chara/akane/doki2.png" jname="キャラ１"]

;キャラ２表情
[chara_face name="yamato" face="tohoho" storage="chara/yamato/tohoho2.png" jname="キャラ２"]
[chara_face name="yamato" face="happy" storage="chara/yamato/happy2.png" jname="キャラ２"]

;BGM：通常
[playbgm storage="normal.ogg"]

;背景
[mask]
[bg storage="seitokai_yu2.jpg" time="0"]
[image layer=1 name="hidari" storage=kuro.jpg visible="true" width=250 height=900 x=0 y=0 folder=bgimage time="0"]
[image layer=1 name="migi" storage=kuro.jpg visible="true" width=300 height=900 x=720 y=0 folder=bgimage time="0"]
[image layer=2 name="akaneb" storage="chara/akane/back.png" visible="true" x=-50 y=50 time="0"]

[chara_show name="akane" left=250 top=50 zindex=2  width="300" height="600"]

[mask_off]

;メニューボタン
@showmenubutton

#キャラ１ 
鏡よ、鏡よ、鏡さん[l]

[cm]

#akane:happy
#キャラ１ 
世界で一番美しいのはだーれ？[l]

[cm]

[image layer=2 name="yamatob" storage="chara/yamato/back.png" visible="true" x=600 y=0 time="100" wait="false"]
[chara_show name="yamato" left=300 top=0 zindex=1 width="562.5" height="675" face="tohoho"]


#キャラ２ 
……おまえ、いったい何やってるんだ？？[l]

[cm]

#akane:doki
#キャラ１ 
キャ、キャラ２！？[l]

[cm]

#キャラ２ 
あまりにも真剣にやってるから、ちょっと声かけ辛くてな……[l]

[cm]

#キャラ１ 
あはは……[l]

[cm]

[mask]
[bg storage="seitokai_yu.jpg" time="0"]
[freeimage layer=1]
[freeimage layer=2]
[chara_hide_all time="0"]

[chara_show name="akane" left=450 top=50 time="0" face=default zindex=2]
[chara_show name="yamato" left=50 top=0 time="0" zindex=1]
[mask_off]

#キャラ１ 
実は、演劇の練習をしていたんだ！[l]

[cm]

#akane:happy
#キャラ１ 
あ！　キャラ２も練習手伝ってよ！　これ台本ね！[l]

[cm]
#yamato:tohoho
#キャラ２ 
まだやるって言ってないんだが……[l]

[cm]
#akane:default
#キャラ１ 
まあまあ、そんなこと言わずに……[l]

[cm]
#akane:angry
#キャラ１ 
あとでアイス御馳走するから！[l]

[cm]
#yamato:happy
#キャラ２ 
やらせていただきます！[l]

[cm]
#akane:happy
#キャラ１ 
本当に！　やった！[l]

[cm]
#akane:angry
#キャラ１ 
よーし！　練習再開だ！[l]

[cm]
[stopbgm]

[mask]
[chara_hide_all]
[chara_hide_all layer=1]
[freeimage layer="base"]
#
@layopt layer=message0 visible=false
[mask_off]
[jump storage=title.ks]
