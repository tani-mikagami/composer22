*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[mask time=0]


[position layer="message0" left=20 top=450 width=920 height=150 page=fore visible=true]

[position layer=message0 page=fore margint="35" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=22 x=50 y=460]

[chara_config ptext="chara_name_area"]

@showmenubutton

;あおい定義
[chara_new name="aoi" storage="chara/aoi/normal.png" jname="あおい"]

;あかね定義
[chara_new name="akane" storage="chara/akane/normal2.png" jname="あかね"]

;あおい表情
[chara_face name="aoi" face="happy" storage="chara/aoi/happy.png" jname="あおい"]
[chara_face name="aoi" face="doki" storage="chara/aoi/doki.png" jname="あおい"]
[chara_face name="aoi" face="sad" storage="chara/aoi/sad.png" jname="あおい"]
[chara_face name="aoi" face="angry" storage="chara/aoi/angry.png" jname="あおい"]

;あかね表情
[chara_face name="akane" face="happy" storage="chara/akane/happy2.png" jname="あかね"]
[chara_face name="akane" face="angry" storage="chara/akane/angry2.png" jname="あかね"]
[chara_face name="akane" face="doki" storage="chara/akane/doki2.png" jname="あかね"]
[chara_face name="akane" face="back" storage="chara/akane/back.png" jname="あかね"]
[chara_face name="akane" face="sad" storage="chara/akane/sad2.png" jname="あかね"]
;背景

[bg storage="sand.jpg"]

[chara_show name="akane" left=-500 top=60 time="0" face=back ]
[chara_show name="aoi" left=-900 top=60 time="0" ]

[mask_off time=1000]
[wait time=200]

[chara_move name="akane" anim="true" time="2000" effect="easeInOutSine" wait="true" left="450" ]
[chara_move name="aoi" anim="true" time="2000" effect="easeInOutSine" wait="true" left="50" ]

[wait time=200]

@layopt layer=message0 visible=true

#akane:sad
#あかね　
あおいちゃん、海まで来て制服って、ないと思うんだ……[l]
[cm]
[chara_mod name="aoi"  face="angry" time="0"]
[chara_move name="aoi" anim="true" time="150" effect="easeOutQuad" wait="true" top="80" ]
[chara_move name="aoi" anim="true" time="150" effect="easeOutBounce" wait="true" top="60" ]
#あおい　
なに言ってるの？学生はどんな時でも制服を着用するものよ！[l]
[cm]

#aoi:happy
#あおい　
あかねも制服に着替えましょう！[l]
[cm]

#akane:angry
#あかね　
やだよ……、私は海で泳いてくる！[l]
[cm]
[chara_mod name="aoi"  face="doki" wait="false" ]
[chara_mod name="akane"  face="back" time="0"]
[chara_move name="akane" anim="true" time="1000" effect="easeInOutSine" wait="true" left="1000" ]
[chara_mod name="akane"  face="happy" time="0"]
[chara_hide name="aoi" ]

[chara_move name="akane" anim="false" time="1000" wait="true" left="250" ]

#あかね　
じゃあね、あおいちゃん！[l]
[cm]
[layopt layer="message0" visible="false"]
[chara_hide name="akane" ]
[bg storage="kuro.jpg" time="2000" wait="true" cross="true" method="fadeIn" ]

[layopt layer="message0" visible="true" ]
#あおい　
あかね、起きて！　こんなところで寝たら風邪ひくよ！[l]
[cm]

[mask time=0]

[layopt layer="message0" visible="false"]
[bg storage="living.jpg" time="0" ]
[layopt layer="message0" visible="true" ]

[chara_show name="akane" left=450 top=60 time="0" face=sad ]
[chara_show name="aoi" left=50 top=60 time="0" face=angry ]

[mask_off time=2000]

#あかね　
……うぅ……寒い……[l]
[cm]

[chara_move name="akane" anim="true" time="100" effect="easeOutQuad" wait="true" top="80" ]
[chara_move name="akane" anim="true" time="100" effect="easeOutBounce" wait="true" top="60" ]

#あかね　
――っくしゅん！[l]
[cm]

#あおい　
はぁ……、風邪ひいたんじゃない？[l]
[cm]
#aoi:default
#あおい　
ちょっとこっちに来て、おでこ出してみて[l]
[cm]

[chara_mod name="akane"  face="back" time="500"]
[chara_mod name="akane"  face="sad" time="500"]
[chara_mod name="akane"  face="back" time="500"]
[chara_mod name="akane"  face="sad" time="500"]

#あかね　
え？……あれ……私の海は？[l]
[cm]

#aoi:angry
#あおい　
なに訳のわからないこと言ってるの！いいからおでこを出す！[l]
[cm]

[chara_move name="aoi" anim="true" time="500" effect="easeInOutSine" wait="true" left="300" ]

#akane:doki
#あかね　
あ……[l]
[cm]
[chara_move name="akane" anim="true" time="1000" effect="easeInOutSine" wait="true" left="500" ]
#あかね　
……えっと、それは恥ずかしいよ～[l]
[cm]

#あおい　
いいから！　うーん、ちょっと熱っぽい気もするけど……[l]
[cm]

[chara_move name="aoi" anim="true" time="500" effect="easeInOutSine" wait="true" left="350" ]

#あおい　
もう少ししっかりおでこかしなさいよ[l]
[cm]

#aoi:sad
#あおい　
うーん、やっぱり少しありそうな感じね……[l]
[cm]

#akane:doki
[chara_move name="akane" anim="true" time="300" effect="easeInOutSine" wait="false" left="550" ]
#あかね　
だ、大丈夫だから！　そろそろ、お風呂入って寝るから！[l]
[cm]

#akane:happy
#あかね　
あおいちゃんも風邪ひかないように気を付けてね！[l]
[cm]


[mask time=500]

