*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[mask time=0]


[position layer="message0" left=20 top=450 width=920 height=150 page=fore visible=true]

[position layer=message0 page=fore margint="35" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=22 x=50 y=460]

[chara_config ptext="chara_name_area"]

@showmenubutton

;キャラA定義
[chara_new name="aoi" storage="chara/aoi/normal.png" jname="キャラA"]

;キャラB定義
[chara_new name="akane" storage="chara/akane/normal2.png" jname="キャラB"]

;キャラA表情
[chara_face name="aoi" face="happy" storage="chara/aoi/happy.png" jname="キャラA"]
[chara_face name="aoi" face="doki" storage="chara/aoi/doki.png" jname="キャラA"]
[chara_face name="aoi" face="sad" storage="chara/aoi/sad.png" jname="キャラA"]
[chara_face name="aoi" face="angry" storage="chara/aoi/angry.png" jname="キャラA"]

;キャラB表情
[chara_face name="akane" face="happy" storage="chara/akane/happy2.png" jname="キャラB"]
[chara_face name="akane" face="angry" storage="chara/akane/angry2.png" jname="キャラB"]
[chara_face name="akane" face="doki" storage="chara/akane/doki2.png" jname="キャラB"]
[chara_face name="akane" face="back" storage="chara/akane/back.png" jname="キャラB"]

;背景
[bg storage="winter-back022.jpg"]
;[image layer=0 folder="bgimage" name="gaikan" storage="gaikan_hiru.jpg" visible=true top=0 left=0 width=1200 height=675]


@layopt layer=message0 visible=true

[keyframe name="sorosoro"]
  [frame p="  0%" y="0"]
  [frame p=" 50%" y="10"]
  [frame p="100%" y="0"]
[endkeyframe]

[keyframe name="tun"]
  [frame p="  0%" x="0"]
  [frame p=" 50%" x="30"]
  [frame p="100%" x="0"]
[endkeyframe]

[keyframe name="biku"]
  [frame p="  0%" scaleY="1.0"]
  [frame p=" 50%" scaleY="2.0"]
  [frame p="100%" scaleY="1.0"]
[endkeyframe]


;あかね、あおい登場
[chara_show name="akane" face="back" time="0" left=200 top=70]
[mask_off time=1000]
;BGM：通常
[playbgm storage="normal.ogg" volume="50"]

[wait time=500]
[chara_show name="aoi" time="0" left=-100 top=70 layer=1 ]

[anim  name="aoi" time="0" opacity="0"]


[kanim name="aoi" time="500" keyframe="sorosoro" count="2" easing="easeInOutSine"]
[anim  name="aoi" time="1000" opacity="255" left="+=120"]
[wa]
;[stop_kanim name="aoi"]

#キャラA
……………………[l]
[cm]

[kanim name="aoi" time="200" keyframe="tun" count="2" easing="linear"]

#キャラA
……つんつん[l]
[cm]

[chara_mod name="akane"  face="doki" time="0"]
[kanim name="akane" keyframe="biku" time="300" count="1" direction="normal" easing="easeInOutSine"]
[anim  name="akane" time="300" left="+=300"]
#キャラB
ぎゃ～！！[l]
[cm]
[wait time=100]
[stop_kanim name="akane"]
[chara_mod name="akane"  face="angry" time="0"]
[chara_move name="akane" anim="true" time="100" effect="easeOutQuad" wait="true" top="-=20" ]
[chara_move name="akane" anim="true" time="100" effect="easeOutBounce" wait="true" top="+=20" ]
#キャラB
何するのよ！！！　足しびれてたのに～！！！！[l]
[cm]
#aoi:happy
#キャラA
あはは、ごめんごめん[l]
[cm]
[chara_mod name="aoi"  face="default" time="0"]
[chara_move name="aoi" anim="true" time="200" effect="easeOutQuad" wait="true" top="+=20" ]
[chara_move name="aoi" anim="true" time="200" effect="easeOutQuad" wait="true" top="-=20" ]
#キャラA
ねえねえ、これは何？[l]
[cm]
#akane:default
#キャラB
ああ、それね。焼き芋よ[l]
[cm]

[chara_mod name="aoi"  face="happy" time="0"]
[chara_move name="aoi" anim="true" time="150" effect="easeOutQuad" wait="true" top="-=20" ]
[chara_move name="aoi" anim="true" time="150" effect="easeOutBounce" wait="true" top="+=20" ]
[chara_move name="aoi" anim="true" time="150" effect="easeOutQuad" wait="true" top="-=20" ]
[chara_move name="aoi" anim="true" time="150" effect="easeOutBounce" wait="true" top="+=20" ]
#キャラA
へ～、おいしそうだね！[l]
[cm]

#キャラB
今、焼くから待ってなさい[l]
[cm]
;場面暗転？
[mask time=1000 effect="slideInRight" folder="bgimage" graphic="eyecatch.jpg"]
[chara_mod name="aoi"  face="default" time="0"]
[chara_mod name="akane"  face="default" time="0"]
[layermode folder="bgimage" graphic=cgbg.png  opacity="254"time=0 mode="multiply"]

[kanim name="aoi" time="1000" keyframe="sorosoro" count="infinite" easing="easeInOutSine"]

[mask_off time=1000 effect="slideOutLeft"]
#キャラA
……[l]
[cm]
#akane:angry
#キャラB
落ち着かないみたいだけど、どうしたの？[l]
[cm]
[stop_kanim name="aoi"]
[chara_mod name="aoi"  face="happy" time="0"]
[chara_move name="aoi" anim="true" time="150" effect="easeOutQuad" wait="true" top="-=20" ]
[chara_move name="aoi" anim="true" time="150" effect="easeOutBounce" wait="true" top="+=20" ]
#キャラA
そろそろ焼けたかなって……[l]
[cm]
#akane:default
#キャラB
そうね、もう少し時間が掛かると思うわ[l]
[cm]
#aoi:sad
#キャラA
分かった……[l]
[cm]
;場面暗転
[mask time=1000 effect="slideInRight" folder="bgimage" graphic="eyecatch.jpg"]
[chara_mod name="akane"  face="default" time="0"]
[chara_mod name="aoi"  face="default" time="0"]
[free_layermode]

[keyframe name="awawa"]
  [frame p="  0%" skewX=" 4deg"]
  [frame p="100%" skewX="-4deg"]
[endkeyframe]

[mask_off time=1000 effect="slideOutLeft"]
#キャラB
うん、いい感じ！　はい、どうぞ[l]
[cm]
[chara_move name="aoi" anim="true" time="500" effect="easeInOutSine" wait="true" left="+=50" wait="false"]
[chara_move name="akane" anim="true" time="500" effect="easeInOutSine" wait="true" left="-=50" wait="false"]
[wait time=1000]
[chara_move name="aoi" anim="true" time="500" effect="easeInOutSine" wait="true" left="-=50" wait="false"]
[chara_move name="akane" anim="true" time="500" effect="easeInOutSine" wait="true" left="+=50" wait="false"]
[wait time=300]
#キャラB
それじゃあ[l]
[cm]
[chara_mod name="akane"  face="happy" time="0"]
[chara_mod name="aoi"  face="happy" time="0"]
[chara_move name="aoi" anim="true" time="150" effect="easeOutQuad" wait="true" top="-=20" wait="false"]
[chara_move name="aoi" anim="true" time="150" effect="easeOutBounce" wait="true" top="+=20" wait="false"]
[chara_move name="akane" anim="true" time="150" effect="easeOutQuad" wait="true" top="-=20" wait="false"]
[chara_move name="akane" anim="true" time="150" effect="easeOutBounce" wait="true" top="+=20" wait="false"]

#キャラA・B
「「いっただきまーす！」」[l]
[cm]
[wait time=300]
#aoi:doki
[filter layer=1 grayscale=100]
[kanim name="aoi" keyframe="awawa" time="10" count="infinite" direction="alternate" easing="easeInOutSine"]
#キャラA
……う、うぅ～！！！！！[l]
[cm]
[chara_mod name="akane"  face="angry" time="0"]
[chara_move name="akane" anim="true" time="150" effect="easeOutQuad" wait="true" top="-=20" ]
[chara_move name="akane" anim="true" time="150" effect="easeOutBounce" wait="true" top="+=20" ]
#キャラB
……熱いって！？　大変、お水お水！[l]
[cm]

[stopbgm]
[mask time=500]
[stop_kanim name="aoi"]
[chara_hide_all]
[chara_hide_all layer=1]
[freeimage layer="base"]
[free_filter]
#
@layopt layer=message0 visible=false
[mask_off]
[jump storage=title.ks]
