*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

;あかね
[chara_new  name="ch1" storage="chara/akane/normal2.png" jname="あかね"]
[chara_new  name="ch3" storage="chara/akane/normal2.png" jname="あかね"]
;やまと
[chara_new  name="ch2" storage="chara/yamato/normal2.png" jname="やまと"]
[chara_new  name="ch4" storage="chara/yamato/normal2.png" jname="やまと"]


;あかね表情
[chara_face name="ch1" face="normal" storage="chara/akane/normal2.png"]
[chara_face name="ch1" face="angry" storage="chara/akane/angry2.png"]
[chara_face name="ch1" face="doki" storage="chara/akane/doki2.png"]
[chara_face name="ch1" face="happy" storage="chara/akane/happy2.png"]
[chara_face name="ch1" face="sad" storage="chara/akane/sad2.png"]
[chara_face name="ch1" face="back" storage="chara/akane/back.png"]
[chara_face name="ch3" face="normal" storage="chara/akane/normal2.png"]
[chara_face name="ch3" face="angry" storage="chara/akane/angry2.png"]
[chara_face name="ch3" face="doki" storage="chara/akane/doki2.png"]
[chara_face name="ch3" face="happy" storage="chara/akane/happy2.png"]
[chara_face name="ch3" face="sad" storage="chara/akane/sad2.png"]
[chara_face name="ch3" face="back" storage="chara/akane/back.png"]

;やまと表情
[chara_face name="ch2" face="normal" storage="chara/yamato/normal2.png"]
[chara_face name="ch2" face="angry" storage="chara/yamato/angry2.png"]
[chara_face name="ch2" face="tohoho" storage="chara/yamato/tohoho2.png"]
[chara_face name="ch2" face="happy" storage="chara/yamato/happy2.png"]
[chara_face name="ch2" face="sad" storage="chara/yamato/sad2.png"]
[chara_face name="ch2" face="back" storage="chara/yamato/back.png"]
[chara_face name="ch4" face="normal" storage="chara/yamato/normal2.png"]
[chara_face name="ch4" face="angry" storage="chara/yamato/angry2.png"]
[chara_face name="ch4" face="tohoho" storage="chara/yamato/tohoho2.png"]
[chara_face name="ch4" face="happy" storage="chara/yamato/happy2.png"]
[chara_face name="ch4" face="sad" storage="chara/yamato/sad2.png"]
[chara_face name="ch4" face="back" storage="chara/yamato/back.png"]

;背景
[image layer="0" folder="bgimage" name="gaikan" storage="gaikan_hiru.jpg" visible="true" top="0" left="0" width="1200" height="675"]
[image layer="1" folder="bgimage" name="room" storage="room.jpg" visible="true" top="0" left="0" width="1200" height="675"]

[chara_show  name="ch4" left=-100 time=0 layer="0"]
[chara_show  name="ch3" width="400" height="800" left=500 top=55 time=0 opacity=0 layer="0"]

@showmenubutton

[chara_mod  name="ch2" face="back"]
[chara_show  name="ch2" left=200 top=5 layer="1"]
[wait time=1000]
[chara_hide name="ch2" layer="1"]

[camera x=150 zoom=1.7 time=1000] 

[chara_mod  name="ch1" face="doki"]
[chara_show  name="ch1" width="200" height="400" left=650 top=150 layer="1"]

[anim name=ch1 top="-=10" time=100]
[wait time=100]
[anim name=ch1 top="+=10" time=100]
[wait time=100]

@layopt layer=message0 visible=true

#あかね
やまとがすごく真剣な顔をしてる……。何があったの！？[p]

@layopt layer=message0 visible=false 

[anim name=ch1 left="-=50" time=200 opacity=255]
[wait time=200]
[chara_hide name="ch1" time=300 opacity=0 layer="1"]
[wait time=300]

[camera x=0 zoom=1 time=800]

[chara_mod  name="ch1" face="doki" time=0]
[chara_mod  name="ch2" face="tohoho" time=0]

[chara_show  name="ch2" left=-100 layer="1"]
[chara_show  name="ch1" width="400" height="800" left=550 top=55 time=0 opacity=0 layer="1"]
[anim name="ch1" time=0  opacity=0]
[anim name=ch1 left="-=50" effect="easeInQuint" time=400 opacity=255]
[wait time=800]

@layopt layer=message0 visible=true

#やまと
SNSにどの写真載せようか悩んでるんだよ……[p]

[chara_mod  name="ch1" face="happy"]

#あかね
楽しそうなの載せたら良いと思うな！[p]

[chara_mod  name="ch2" face="happy"]

#やまと
じゃあ一緒に撮ろうぜ！[p]

[chara_mod  name="ch1" face="normal"]

#あかね
でも、ただ載せるんじゃ面白くないよ？[p]

[chara_mod  name="ch2" face="tohoho"]

#やまと
じゃあ……[p]

[chara_mod  name="ch1" face="happy"]
[anim name=ch1 top="-=10" time=100]
[wait time=100]
[anim name=ch1 top="+=10" time=100]
[wait time=100]

#あかね
あ！　やまと新しい靴履いてたよね！[r]
私も買ったばかりだから、靴履いて写真撮ってみない？[p]

[mask time=1500 effect="fadeInLeftBig"]
@layopt layer=message0 visible=false 
[freeimage layer=1 time=0]
[chara_mod  name="ch4" face="happy" time=0]
[mask_off time=1500] 
@layopt layer=message0 visible=true

#やまと
じゃあ、撮るぜ！[p]

[chara_mod  name="ch3" face="normal"]

#あかね
ちょっと待て！　やっぱり、もう少し靴を近くした方がよくない？[p]

@layopt layer=message0 visible=false

[chara_hide name="ch3" time=0]
[chara_hide name="ch4" time=0]

[image layer="0" folder="bgimage" name="gaikan" storage="gaikan_hiru.jpg" visible="true" top="-50" left="0" width="1200" height="675"]
[image layer="1" folder="bgimage" name="kuro" storage="kuro.jpg" visible="true" top="450" left="0" time="100" wait=false]
[image layer="1" folder="bgimage" name="kuro" storage="kuro.jpg" visible="true" top="-1020" left="0" time="100" wait=true]

[chara_show name="ch4" left=-40 top=-340 wait=false]
[chara_show name="ch3" left=400 top=-250 wait=true]

[anim name=ch4 left="+=20" effect="easeInOutQuint" time=1000]
[wait time=1000]

@layopt layer=message0 visible=true

#やまと
こんな感じか？[p]

#あかね
そうそう！[p]

#やまと
なら……はい、チーズ[p]

[mask color=0xFFFFFF time=20]
@layopt layer=message0 visible=false
[freeimage layer=1]
[image layer="0" folder="bgimage" name="gaikan" storage="gaikan_hiru.jpg" visible="true" top="0" left="0" width="1200" height="675"]
[chara_hide name="ch3" time=0]
[chara_hide name="ch4" time=0]
[camera x=0 y=0 zoom=1 time=0]
[chara_mod  name="ch3" face="normal"]
[chara_mod  name="ch4" face="happy"]
[chara_show  name="ch4" left=-100 top=0 time=0]
[chara_show  name="ch3" left=500 top=55 time=0]
[mask_off time=2000]

@layopt layer=message0 visible=true

#あかね
いい感じの写真撮れた？[p]

#やまと
これならSNSに載せられそうだな！[p]

[mask]
@layopt layer=message0 visible=false
@hidemenubutton
[chara_hide_all]
[freeimage layer="base"]
[mask_off]
[jump first.ks]

 