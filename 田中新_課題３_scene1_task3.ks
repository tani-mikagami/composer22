
;■条件
;・キャラクターはあかね、やまとを使用
;・やまとの表情はnormal、angry使用不可
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・真剣な表情がない中でどう演出するか
;・後半の靴の話でどのような演出をするか

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

;————————以下よりスクリプトを組んでください————————

;キャラ１定義
[chara_new name="akane" storage="chara/akane/normal.png" jname="あかね"]

;キャラ２定義
[chara_new name="yamato" storage="chara/yamato/tohoho.png" jname="やまと"]

;キャラ１表情
[chara_face name="akane" face="normal" storage="chara/akane/normal.png" jname="あかね"]
[chara_face name="akane" face="happy" storage="chara/akane/happy.png" jname="あかね"]
[chara_face name="akane" face="doki" storage="chara/akane/doki.png" jname="あかね"]
[chara_face name="akane" face="sad" storage="chara/akane/sad.png" jname="あかね"]
[chara_face name="akane" face="angry" storage="chara/akane/angry.png" jname="あかね"]

;キャラ２表情
;[chara_face name="yamato" face="normal" storage="chara/yamato/normal.png" jname="やまと"]
;[chara_face name="yamato" face="angry" storage="chara/yamato/angry.png" jname="やまと"]
[chara_face name="yamato" face="happy" storage="chara/yamato/happy.png" jname="やまと"]
[chara_face name="yamato" face="sad" storage="chara/yamato/sad.png" jname="やまと"]
[chara_face name="yamato" face="tohoho" storage="chara/yamato/tohoho.png" jname="やまと"]
[chara_face name="yamato" face="back" storage="chara/yamato/back.png" jname="やまと"]

;背景
[bg storage="gaikan_hiru.jpg" time="1500"]

;メニューボタン
@showmenubutton

[playbgm storage="dozikkomarch.ogg"]

[chara_mod name="akane"  face="doki"]
[chara_mod name="yamato"  face="back"]
[chara_show name="yamato" left=0 top=0]
[chara_show name="akane" left=480 top=50]

@layopt layer=message0 visible=true

;【田中新】「何あったの！？」表記、“が”脱字していると思われるので「何があったの！？」に修正
#あかね 
やまとがすごく真剣な顔をしてる……[r]
何があったの！？[l]

[cm]

#やまと
SNSにどの写真載せようか[r]
悩んでるんだよ……[l]

[cm]

[chara_mod name="akane"  face="happy"]
#あかね
楽しそうなの載せたら[r]
良いと思うな！[l]

[cm]

@layopt layer=message0 visible=false

[anim name=yamato left="+=60" time=1000 effect="easeInSine"]
[wait time=500]

[wa]

@layopt layer=message0 visible=true
[chara_mod name="yamato"  face="happy"]
#やまと
じゃあ一緒に撮ろうぜ！[l]

[cm]

[chara_mod name="akane"  face="normal"]
#あかね
でも、ただ載せるんじゃ[r]
面白くないよ？[l]

[cm]

[chara_mod name="yamato"  face="sad"]
#やまと
じゃあ……[l]

[cm]

[chara_mod name="akane"  face="happy"]
#あかね
あ！　やまと新しい靴履いてたよね！[r]
私も買ったばかりだから、[r]
靴履いて写真撮ってみない？[l]

[cm]

@layopt layer=message0 visible=false

[wait time=500]
[chara_mod name="yamato"  face="happy"]

[anim name=yamato top="+=75" time=50 effect="easeInSine"]
[wait time=200]

[anim name=yamato top="-=75" time=50 effect="easeInSine"]
[wait time=1000]

[mask time=1000]
[mask_off time=1000]
@layopt layer=message0 visible=true
#やまと
じゃあ、撮るぜ！[l]

[cm]

[chara_mod name="akane"  face="normal"]
#あかね
ちょっと待って！[r]
やっぱり、もう少し靴を[r]
近くした方がよくない？[l]

[cm]

@layopt layer=message0 visible=false

[wait time=500]
[chara_mod name="yamato"  face="back"]

[anim name=yamato left="+=30" time=500 effect="easeInSine"]
[wait time=200]

[anim name=yamato left="-=30" time=500 effect="easeInSine"]
[wait time=500]

[wa]

@layopt layer=message0 visible=true
#やまと
こんな感じか？[l]

[cm]

@layopt layer=message0 visible=false

[wait time=500]

[anim name=akane top="+=75" time=50 effect="easeInSine"]
[wait time=200]

[anim name=akane top="-=75" time=50 effect="easeInSine"]

[wait time=1000]

[wa]

[chara_mod name="yamato"  face="happy"]
@layopt layer=message0 visible=true
#やまと
なら……[r]
はい、チーズ[l]

[cm]

[playse storage="slash.ogg"]
[wait time=1500]

[chara_mod name="akane"  face="happy"]
#あかね
いい感じの写真撮れた？[l]

[cm]

@layopt layer=message0 visible=false

[wait time=500]

[anim name=yamato top="+=75" time=50 effect="easeInSine"]
[wait time=200]

[anim name=yamato top="-=75" time=50 effect="easeInSine"]

[wait time=1000]

[wa]

@layopt layer=message0 visible=true
#やまと
これならSNSに[r]
載せられそうだな！[l]
[mask]
[chara_hide_all]
[chara_hide_all layer=1]
[freeimage layer="base"]
#
@layopt layer=message0 visible=false
[mask_off]
[jump first.ks]
 